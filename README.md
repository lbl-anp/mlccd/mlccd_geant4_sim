
# mlccd GEANT4 CCD simulations

  
  
  

## Overview

  

This repository holds code for Geant4 simulations of CCD particle detectors used by the **mlccd** (Machine Learning Charge-Coupled Devices) and **QuIPS** (Quantum Invisible Particle Sensor) projects.

  

## Particle energy deposition in CCD

The particle simulations are done using Geant4. The code in Geant4_CCD_simulation generates particles on the surface of a Si block and logs energy deposited for each simulation step. 

After installing Geant4 and ROOT, or building the included docker image (Geant4 10.5.1 and Python 3.6.9), build the simulation like

    cd Geant4_CCD_simulation
    mkdir build
    cd build
    cmake ..
    make

The simulation can be run with with `./Geant4Simulation run_config.mac`, with run_config.mac containing for instance
```
/myapp/useRadioactiveIon true
/myapp/setIonZA 27 60
/run/beamOn 10
```
Resulting in an output file `Co60_ion.root` containing the CCD energy deposition tracks for 10 Co-60 decays.

The recommended way to run simulations is using the included python script like `python3 batch_simulate_CCD.py batch_particles.csv`

Multiple particles and energies can be set in a csv file as in the example `batch_particles.csv`


| Particle | Energy  | Particles | SetAngle | Phi | Theta |
|----------|---------|-----------|----------|-----|-------|
| H3       | 0       | 10000     | 0        | 0   | 0     |
| mu+      | 4000000 | 1000      | 1        | -20 | 30    |
| e-       | 300     | 1000      | 1        | -50 | 30    |
| gamma    | 1000    | 10000     | 0        | 0   | 0     |


Where the columns list particle kind, energy (in keV), number of particles or decays for each particle kind, and if a set angle is to be used. Note that for radioactive ions, the `Energy` and angle values have no impact - the ion is created at rest and decays will dictate the angles of decay products.If `SetAngle` is set to `0`, angles will be sampled uniformly over 4 pi.

The saved tracks are saved as .root files in `Geant4_CCD_simulation/output`.


## Charge diffusion in CCD

The diffusion code that lives in the [mlccd_diffusion](https://gitlab.com/lbl-anp/mlccd_diffusion) repo performs drift and diffusion of the energy depositions simulated in Geant4 to recreate how an actual particle image taken with a CCD might look. The repo also contains python code to load and analyze the produced .root files from these simulations.