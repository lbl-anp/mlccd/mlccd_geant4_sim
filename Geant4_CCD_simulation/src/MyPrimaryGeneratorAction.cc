#include "MyPrimaryGeneratorAction.hh"
#include "MyParticleUserInformation.hh"
#include "MyPrimaryGeneratorMessenger.hh"
#include "G4Event.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "G4PrimaryParticle.hh"
#include "G4RandomDirection.hh"
#include "G4GeneralParticleSource.hh"
#include "G4SPSEneDistribution.hh"
#include <iomanip>  // Required for setprecision
#include "G4IonTable.hh"

MyPrimaryGeneratorAction* MyPrimaryGeneratorAction::instance = 0;

MyPrimaryGeneratorAction* MyPrimaryGeneratorAction::Instance() {
    if (instance == 0) {
        instance = new MyPrimaryGeneratorAction();
    }
    return instance;
}

MyPrimaryGeneratorAction::MyPrimaryGeneratorAction()
: G4VUserPrimaryGeneratorAction(),
  fParticleGun(0),
  trackID(1),
  messenger(0),
  useRadioactiveIon(false),
  useSpecifiedAngles(false),
  particleAngles(0, 0, 0),
  particleEnergy("0.0"),
  particleName("particlename"),
  primaryEnergy(0.0),
  primaryKind(1)
{
    messenger = new MyPrimaryGeneratorMessenger(this);
    fParticleGun = new G4ParticleGun(1);
    fGeneralParticleSource = new G4GeneralParticleSource();
    G4cout << "MyPrimaryGeneratorAction()" << G4endl;
}

MyPrimaryGeneratorAction::~MyPrimaryGeneratorAction() {
    delete fParticleGun;
    delete messenger;
    delete fGeneralParticleSource;
}

void MyPrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent) {
    if (useRadioactiveIon) {
        fParticleGun->SetParticleDefinition(ion);
        fParticleGun->SetParticleEnergy(0.0 * MeV);  // At rest
        fParticleGun->SetParticlePosition(G4ThreeVector(0., 0., 0.));
        fParticleGun->GeneratePrimaryVertex(anEvent);
        // fGeneralParticleSource->SetParticlePosition(G4ThreeVector(0., 0., 0.));
        // fGeneralParticleSource->GeneratePrimaryVertex(anEvent);
    }
    else {
        fParticleGun->SetParticlePosition(G4ThreeVector(0., 0., 0.));
        G4ThreeVector direction;
        if (useSpecifiedAngles) {
            double phi = particleAngles.x();
            double theta = particleAngles.y();
            // G4cout << "Using specified angles: phi = " << phi << ", theta = " << theta << G4endl;
            // convert degrees to radians for use in trigonometric functions
            phi = phi * M_PI / 180.;
            theta = theta * M_PI / 180.;
            double dx = std::sin(theta) * std::cos(phi);
            double dy = std::sin(theta) * std::sin(phi);
            double dz = std::cos(theta);
            direction = G4ThreeVector(dx, dy, dz);
        } else {
            // do {
            direction = G4RandomDirection();
            // } while (direction.z() <= 0);
            G4cout << "Using random angle" << G4endl;
        }
        fParticleGun->SetParticleMomentumDirection(direction);
        fParticleGun->GeneratePrimaryVertex(anEvent);
    }
    G4PrimaryParticle* primaryParticle = anEvent->GetPrimaryVertex()->GetPrimary(0);
    primaryEnergy = primaryParticle->GetKineticEnergy();
    primaryKind = primaryParticle->GetParticleDefinition()->GetPDGEncoding();
    particleName = primaryParticle->GetParticleDefinition()->GetParticleName();
    std::stringstream stream;
    stream << std::fixed << std::setprecision(2) << primaryParticle->GetKineticEnergy() / keV << "keV";
    particleEnergy = stream.str();
    primaryParticle->SetUserInformation(new MyParticleUserInformation(trackID, primaryParticle->GetPDGcode(), primaryParticle->GetKineticEnergy()));
    // primaryParticle->GetUserInformation()->Print();
    trackID++;
}

void MyPrimaryGeneratorAction::SetParticleAngles(G4ThreeVector const& newAngles) {
    particleAngles = newAngles;
    useSpecifiedAngles = true;
}

void MyPrimaryGeneratorAction::SetIonZA(G4int Z, G4int A) {
    G4cout << "Setting ion to Z = " << Z << " A = " << A << G4endl;
    G4IonTable* ionTable = G4IonTable::GetIonTable();
    ion = ionTable->GetIon(Z, A, 0);
}

G4String MyPrimaryGeneratorAction::GetParticleInfo() {
    if (useRadioactiveIon) {
        particleEnergy =  "ion";
    }
    std::stringstream angles;
    if (useSpecifiedAngles) {
        angles << particleAngles.x() << "xy_" << particleAngles.y() << "vert";
    }
    std::stringstream ss;
    ss << particleName << "_" << particleEnergy << angles.str();
    return ss.str();
}

void MyPrimaryGeneratorAction::UseRadioactiveIon(G4bool useIon) {
    useRadioactiveIon = useIon;
}