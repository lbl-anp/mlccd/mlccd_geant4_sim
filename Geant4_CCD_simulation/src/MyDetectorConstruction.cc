#include "MyDetectorConstruction.hh"

#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4SystemOfUnits.hh"
#include "G4UserLimits.hh"
#include "G4Region.hh"
#include "G4VisAttributes.hh"
#include <sstream>

MyDetectorConstruction::MyDetectorConstruction() : G4VUserDetectorConstruction() {}

MyDetectorConstruction::~MyDetectorConstruction() {}

G4VPhysicalVolume* MyDetectorConstruction::Construct() {
    // Define the world volume
    G4double worldSizeXY = 4.0 * cm;
    G4double worldSizeZ = 4.0 * cm;
    G4Material* worldMaterial = G4NistManager::Instance()->FindOrBuildMaterial("G4_Galactic");  // vacuum

    G4Box* worldBox = new G4Box("World", worldSizeXY / 2, worldSizeXY / 2, worldSizeZ / 2);
    worldLogVol = new G4LogicalVolume(worldBox, worldMaterial, "World");
    G4VPhysicalVolume* worldPhysVol = new G4PVPlacement(0, G4ThreeVector(), worldLogVol, "World", 0, false, 0);

    // Define the Si detector
    G4double siThickness = 625 * um;
    G4double siSizeXY = 1.0 * cm;  // 1cm x 1cm detector
    G4Material* siMaterial = G4NistManager::Instance()->FindOrBuildMaterial("G4_Si");

    G4Box* siBox = new G4Box("Si", siSizeXY / 2, siSizeXY / 2, siThickness / 2);
    G4LogicalVolume* siLogVol = new G4LogicalVolume(siBox, siMaterial, "Si");

    // Set the user limits for maximum step size in the Si volume
    G4UserLimits* userLimits = new G4UserLimits();
    G4double limitStep = 10.0 * nm;
    // G4double limitStep = 0.1 * um;
    userLimits->SetMaxAllowedStep(limitStep);
    siLogVol->SetUserLimits(userLimits);
    G4VisAttributes* siVisAtt= new G4VisAttributes(G4Colour(1.0,0.6,0.0));
    siVisAtt->SetVisibility(true);
    siLogVol->SetVisAttributes(siVisAtt);

    // Position the Si detectors around the source
    // G4double sourceOffset = 0.499 * cm;  // Offset of the center of the detector box from the source (-0.2 for dice geometry)
    G4double sourceOffset = -0.2 * cm;  // Offset of the center of the detector box from the source (0.499 for source in front of single detector)
    std::vector<std::pair<G4ThreeVector, G4RotationMatrix>> placements = {
        {G4ThreeVector(0, 0, siSizeXY / 2 + siThickness / 2 - sourceOffset), G4RotationMatrix()},  // 1 front, no rotation
        {G4ThreeVector(-siSizeXY / 2 - siThickness / 2, 0, -sourceOffset), G4RotationMatrix().rotateY(90 * deg)},  // 2 left
        {G4ThreeVector(0, -siSizeXY / 2 - siThickness / 2, -sourceOffset), G4RotationMatrix().rotateX(-90 * deg)},  // 3 bottom
        {G4ThreeVector(0, siSizeXY / 2 + siThickness / 2, -sourceOffset), G4RotationMatrix().rotateX(90 * deg)},  // 4 top
        {G4ThreeVector(siSizeXY / 2 + siThickness / 2, 0, -sourceOffset), G4RotationMatrix().rotateY(-90 * deg)},  // 5 right
    };
        // {G4ThreeVector(0, 0, -siSizeXY / 2 - siThickness / 2 - sourceOffset), G4RotationMatrix().rotateY(180 * deg)}  // 6 back


    std::stringstream nameStream;

    for (size_t i = 0; i < placements.size(); ++i) {
        nameStream.str(""); // Clear the stringstream
        nameStream << "Si" << i+1; // Construct the name
        G4cout << "Naming to " << nameStream.str() << G4endl;
        new G4PVPlacement(new G4RotationMatrix(placements[i].second), placements[i].first, siLogVol, nameStream.str(), worldLogVol, false, i);
    }

    // Create a region for Si and add the logical volume to it
    G4Region* siRegion = new G4Region("Si");
    siLogVol->SetRegion(siRegion);
    siRegion->AddRootLogicalVolume(siLogVol);

    return worldPhysVol;
}

// void MyDetectorConstruction::SetMaxStepSize(G4double step) {
//     fMaxStepSize = step;
//     G4int daughters = worldLogVol->GetNoDaughters();

//     for (G4int i = 0; i < daughters; i++) {
//         G4VPhysicalVolume* daughter = worldLogVol->GetDaughter(i);
//         G4cout << daughter->GetName() << G4endl;
//         if (daughter->GetName() == "Si") {
//             daughter->GetLogicalVolume()->SetUserLimits(new G4UserLimits(fMaxStepSize));
//         }
//     }
// }
