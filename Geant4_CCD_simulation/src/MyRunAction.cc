#include "MyRunAction.hh"
#include "G4Run.hh"
#include "G4RunManager.hh"
#include "MyPrimaryGeneratorAction.hh"

MyRunAction::MyRunAction(MyPrimaryGeneratorAction* generatorAction) 
    : G4UserRunAction(), fGeneratorAction(generatorAction) {

    fTree = new TTree("EnergyDeposition", "Energy deposition data");
    fTree->Branch("detectorID", &fDetectorID, "detectorID/I");
    fTree->Branch("energyDeposition", &fEnergyDep, "energyDeposition/D");
    fTree->Branch("x", &fX, "x/D");
    fTree->Branch("y", &fY, "y/D");
    fTree->Branch("z", &fZ, "z/D");
    fTree->Branch("primaryID", &fPrimaryID, "primaryID/I");
    fTree->Branch("primaryKind", &fPrimaryKind, "primaryKind/I");
    fTree->Branch("primaryEnergy", &fPrimaryEnergy, "primaryEnergy/D");
    fTree->Branch("stepKind", &fStepKind, "stepKind/I");
    fTree->Branch("preStepEnergy", &fPreStepEnergy, "preStepEnergy/D");
}

MyRunAction::~MyRunAction() {
    if (fFile != nullptr) {
        delete fFile;
        fFile = nullptr;
    }
}

void MyRunAction::BeginOfRunAction(const G4Run* /*aRun*/) {
    fEnergyDep = 0.0;
    fDetectorID = 0;
}

void MyRunAction::EndOfRunAction(const G4Run* /*aRun*/) {
    G4String particleInfo = fGeneratorAction->GetParticleInfo();
    std::stringstream ss;
    ss << "../output/" << particleInfo << ".root";
    G4String filename = ss.str();
    G4cout << "Writing to file: " << filename << G4endl;
    fFile = new TFile(filename.c_str(), "RECREATE");
    fTree->Write();
    fFile->Close();
    
    if (fFile != nullptr) {
        delete fFile;
        fFile = nullptr;
    }
}
