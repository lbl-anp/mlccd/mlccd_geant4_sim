#include "MyPrimaryGeneratorMessenger.hh"
#include <sstream>

MyPrimaryGeneratorMessenger::MyPrimaryGeneratorMessenger(MyPrimaryGeneratorAction* action)
: G4UImessenger(),
  fAction(action),
  useRadioactiveIonCmd(0),
  setParticleAnglesCmd(0)
{
    useRadioactiveIonCmd = new G4UIcmdWithABool("/myapp/useRadioactiveIon", this);
    useRadioactiveIonCmd->SetGuidance("Use radioactive ion's energy spectrum");
    useRadioactiveIonCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

    setParticleAnglesCmd = new G4UIcmdWith3Vector("/myapp/setParticleAngles", this);
    setParticleAnglesCmd->SetGuidance("Set phi and theta angles for particles");
    setParticleAnglesCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

    setIonZACmd = new G4UIcmdWithAString("/myapp/setIonZA", this);
    setIonZACmd->SetGuidance("Set Z and A for the ion");
    setIonZACmd->AvailableForStates(G4State_PreInit, G4State_Idle);
}

MyPrimaryGeneratorMessenger::~MyPrimaryGeneratorMessenger() {
    delete useRadioactiveIonCmd;
    delete setParticleAnglesCmd;
    delete setIonZACmd;
}

void MyPrimaryGeneratorMessenger::SetNewValue(G4UIcommand* command, G4String newValue) {
    if (command == useRadioactiveIonCmd) {
        fAction->UseRadioactiveIon(useRadioactiveIonCmd->GetNewBoolValue(newValue));
    }
    else if (command == setParticleAnglesCmd) {
        fAction->SetParticleAngles(setParticleAnglesCmd->GetNew3VectorValue(newValue));
    }
    else if (command == setIonZACmd) {
        std::istringstream iss(newValue.data());
        int Z, A;
        if (iss >> Z >> A) {
            fAction->SetIonZA(Z, A);
        }
    }
}
