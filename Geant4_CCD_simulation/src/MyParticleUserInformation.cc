#include "MyParticleUserInformation.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"

MyParticleUserInformation::MyParticleUserInformation(G4int id, G4int kind, G4double energy)
    : fPrimaryID(id), fPrimaryKind(kind), fPrimaryEnergy(energy) {}

MyParticleUserInformation::~MyParticleUserInformation() {}

void MyParticleUserInformation::Print() const {
    G4cout << "Particle ID: " << fPrimaryID << ", kind: " << fPrimaryKind << ", energy: " << G4BestUnit(fPrimaryEnergy,"Energy") << G4endl;
}
