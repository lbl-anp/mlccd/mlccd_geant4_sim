#include "MySteppingAction.hh"
#include "G4Step.hh"
#include "G4Track.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "MyParticleUserInformation.hh"
#include "G4PrimaryParticle.hh"
#include "G4UserLimits.hh"
#include "MyPrimaryGeneratorAction.hh"
#include <string>  // required for std::stoi
#include <algorithm> // For std::min
#include "G4Box.hh"

MySteppingAction::MySteppingAction(MyRunAction* runAction)
: G4UserSteppingAction(),
  fRunAction(runAction) {}

MySteppingAction::~MySteppingAction() {}

void MySteppingAction::UserSteppingAction(const G4Step* step) {
    G4Track* track = step->GetTrack();
    G4String particleName = track->GetDefinition()->GetParticleName();
    G4String volumeName = track->GetVolume()->GetName();
    G4ParticleDefinition* particleDefinition = track->GetDefinition();
    G4int Z = particleDefinition->GetAtomicNumber();
    G4int A = particleDefinition->GetAtomicMass();

    // Log steps only for particles in the CCDs 
    // or if the particle is a recoiling nucleus
    if (volumeName.contains("Si") || (Z > 0 && A > 1)) {
        G4double energyDeposit = step->GetTotalEnergyDeposit();
        G4ThreeVector globalPosition = step->GetPostStepPoint()->GetPosition();

        // Get the touchable history
        const G4TouchableHistory* theTouchable = (const G4TouchableHistory*)(track->GetTouchable());
        // Transform the global coordinates to local coordinates
        G4ThreeVector localPosition = theTouchable->GetHistory()->GetTopTransform().TransformPoint(globalPosition);
        // if this is a recoiling nucleus, set the detector ID to 99
        if (Z > 0 && A > 1){
            fRunAction->fDetectorID = 99;
        }
        else{
            G4String detectorNumberString = volumeName;
            detectorNumberString.erase(0, 2); // Remove "Si" prefix
            fRunAction->fDetectorID = std::stoi(detectorNumberString); // Convert remaining string to integer
        }
        fRunAction->fEnergyDep = energyDeposit / keV;
        fRunAction->fPreStepEnergy = step->GetPreStepPoint()->GetKineticEnergy() / keV;
        fRunAction->fX = localPosition.x() / um;
        fRunAction->fY = localPosition.y() / um;
        G4Box* box = dynamic_cast<G4Box*>(track->GetVolume()->GetLogicalVolume()->GetSolid());
        if (box) {  // Make sure the cast was successful
            G4double halfThickness = std::min(std::min(box->GetXHalfLength(), box->GetYHalfLength()), box->GetZHalfLength());
            fRunAction->fZ = (localPosition.z() + halfThickness) / um;
        }


        MyPrimaryGeneratorAction* generatorAction = MyPrimaryGeneratorAction::Instance();
        fRunAction->fStepKind = track->GetDefinition()->GetPDGEncoding();
        fRunAction->fPrimaryEnergy = generatorAction->GetPrimaryEnergy() / keV;
        fRunAction->fPrimaryKind = generatorAction->GetPrimaryKind();
        fRunAction->fPrimaryID = generatorAction->GetTrackID();
        // Write this steps information to the tree if energy deposit is greater than 0
        // if (energyDeposit > 0) fRunAction->fTree->Fill();
        // Removed the energy deposit check to also get neutrino events
        fRunAction->fTree->Fill();
    }
}
