#ifndef MYRUNACTION_HH
#define MYRUNACTION_HH

#include "G4UserRunAction.hh"
#include "TFile.h"
#include "TTree.h"

class G4Run;
class MyPrimaryGeneratorAction; // Forward declaration of MyPrimaryGeneratorAction

class MyRunAction : public G4UserRunAction {
public:
    MyRunAction(MyPrimaryGeneratorAction* generatorAction); // Updated constructor
    virtual ~MyRunAction();

    virtual void BeginOfRunAction(const G4Run* aRun);
    virtual void EndOfRunAction(const G4Run* aRun);

    TFile* fFile;
    TTree* fTree;
    G4double fEnergyDep;
    G4int fDetectorID;
    G4double fX, fY, fZ;
    G4int fPrimaryID;
    G4int fPrimaryKind;
    G4double fPrimaryEnergy;
    G4double fPreStepEnergy;
    G4int fStepKind;

private:
    MyPrimaryGeneratorAction* fGeneratorAction; // Add a pointer to MyPrimaryGeneratorAction
};

#endif
