#ifndef MYPARTICLEGENERATORMESSENGER_HH
#define MYPARTICLEGENERATORMESSENGER_HH

#include "G4UImessenger.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWith3Vector.hh"
#include "MyPrimaryGeneratorAction.hh"
#include "G4UIcmdWithAString.hh"

class MyPrimaryGeneratorMessenger : public G4UImessenger {
public:
    MyPrimaryGeneratorMessenger(MyPrimaryGeneratorAction* action);
    virtual ~MyPrimaryGeneratorMessenger();

    virtual void SetNewValue(G4UIcommand* command, G4String newValue);

private:
    MyPrimaryGeneratorAction* fAction;
    G4UIcmdWithABool* useRadioactiveIonCmd;
    G4UIcmdWith3Vector* setParticleAnglesCmd;
    G4UIcmdWithAString* setIonZACmd;
};

#endif
