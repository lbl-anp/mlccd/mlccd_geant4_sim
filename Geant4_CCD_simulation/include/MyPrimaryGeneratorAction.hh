#ifndef MYPRIMARYGENERATORACTION_HH
#define MYPRIMARYGENERATORACTION_HH

#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4ParticleGun.hh"
#include "G4GeneralParticleSource.hh"
#include <vector>
#include "G4ParticleDefinition.hh"

class G4Event;
class MyPrimaryGeneratorMessenger;

class MyPrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction {
public:
    static MyPrimaryGeneratorAction* Instance();

    virtual void GeneratePrimaries(G4Event* anEvent);

    void UseRadioactiveIon(G4bool useIon);
    void SetParticleAngles(const G4ThreeVector& angles);
    void SetIonZA(G4int Z, G4int A);
    G4String GetParticleInfo();
    G4int GetTrackID() { return trackID; }
    G4int GetPrimaryKind() { return primaryKind; }
    G4double GetPrimaryEnergy() { return primaryEnergy; }

private:
    static MyPrimaryGeneratorAction* instance;
    
    MyPrimaryGeneratorAction();
    virtual ~MyPrimaryGeneratorAction();

    MyPrimaryGeneratorAction(const MyPrimaryGeneratorAction&) = delete;
    MyPrimaryGeneratorAction& operator=(const MyPrimaryGeneratorAction&) = delete;

    G4ParticleGun* fParticleGun;
    G4GeneralParticleSource* fGeneralParticleSource;
    G4int trackID;
    MyPrimaryGeneratorMessenger* messenger;
    G4bool useRadioactiveIon;
    G4bool useSpecifiedAngles;
    G4ThreeVector particleAngles;
    G4String particleName;
    G4String particleEnergy;
    G4double primaryEnergy;
    G4int primaryKind;
    G4int Z;
    G4int A;
    G4ParticleDefinition* ion;
};

#endif
