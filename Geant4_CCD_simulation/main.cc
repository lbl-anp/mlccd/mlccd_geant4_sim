#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "Randomize.hh"
#include "G4VisExecutive.hh"
#include "G4UIExecutive.hh"

#include "MyDetectorConstruction.hh"
#include "MyPhysicsList.hh"
#include "MyPrimaryGeneratorAction.hh"
#include "MyRunAction.hh"
#include "MySteppingAction.hh"
#include "G4SystemOfUnits.hh"

int main(int argc, char** argv) {
    // Create the run manager
    auto runManager = new G4RunManager;

    // Set the detector construction
    auto detectorConstruction = new MyDetectorConstruction;
    runManager->SetUserInitialization(detectorConstruction);

    // Set the physics list
    runManager->SetUserInitialization(new MyPhysicsList);

    // Set the run action
    MyPrimaryGeneratorAction* generatorAction = MyPrimaryGeneratorAction::Instance();
    MyRunAction* runAction = new MyRunAction(generatorAction);

    // Set the primary generator action
    runManager->SetUserAction(generatorAction);

    // Set the run action
    runManager->SetUserAction(runAction);

    // Set the stepping action
    runManager->SetUserAction(new MySteppingAction(runAction));

    // Initialize the run manager
    runManager->Initialize();

    // Initialize visualization
    G4VisManager* visManager = new G4VisExecutive;
    visManager->Initialize();

    // Get the pointer to the User Interface manager
    G4UImanager* UImanager = G4UImanager::GetUIpointer(); 

    if (argc!=1) { 
        // batch mode  
        G4String command = "/control/execute ";
        G4String fileName = argv[1];
        UImanager->ApplyCommand(command+fileName);
    }
    else { 
        // interactive mode : define UI session
        G4UIExecutive* ui = new G4UIExecutive(argc, argv);
        UImanager->ApplyCommand("/control/execute init_vis.mac"); 
        ui->SessionStart();
        delete ui;
    }

    // Job termination
    delete visManager;
    delete runManager;

    return 0;
}
